/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calc;

/**
 *Matthew Chuong
 * CSC 2720
 * Assignment 5
 * The purpose of the calc program is to converts infix expressions to postfix and compute the answer
 * If it contains exceptions it will let the user know then terminate
 * 
 * I have made 5 methods postfix, Comp, isOp, compute, and Check
 * postfix converts the infix expression to postfix
 * Comp returns the value of precedence to operators
 * isOp determines if the char is a operator
 * compute calculates the result from a postfix expression
 * Check makes sure the expression is a legal expression
 * 
 * Data Structure used: Stack and Scanner
 * 
 * Expected input: legal math expression
 * Expected output: converted infix expression and result of the postfix expression
 * I do not expect unary operators, and 2 digit numbers to be in the equation
 */

import java.util.*;
import java.util.Stack;
public class Calc {

    /**
     * @param args the command line arguments
     */
    
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner in = new Scanner(System.in);
        System.out.println("Enter infix exppresion: ");
        String equation = in.nextLine();                        //holds the equation
        equation = equation.replaceAll(" ", "");            //removes spaces
        Check(equation);                                    //checks for errors
        equation = postfix(equation);                       // converts to postfix
        System.out.println("Postfix form: " + equation);    //prints out equation
        if(equation.contains("x"))                          //converts the x to numbers
        {
            String x = "";                                       //holds actual value for x
            while(!x.equals("q"))
            {
                System.out.println("Enter value of x:");
                x = in.next();
                if(x.equals("q"))
                {
                    System.exit(0);
                    break;
                }
                String temp = equation.replaceAll("x", x);
                System.out.println("New updated expression: " + temp);
                System.out.println("Answer to expression: " + compute(temp));
            }
        }
        
                        
    }
    public static String postfix(String a)
    {
        //Pre: legal expression
        //Post: converts infix expression to postfix expression
        Stack<Character> ll = new Stack<>();        
        String b = ""; //correct string to add to
        //ArrayList post = new ArrayList();// to hold the outputs in the right order
        for(int i = 0; i < a.length(); i++)
        {
            if(isOp(a.charAt(i))) //if operand
            {    
                if(ll.empty()) //if stack is empty push operand
                {
                    ll.push(a.charAt(i));
                }
            
                   else if(Comp(a.charAt(i)) <= Comp(ll.peek()))//if current operator has lower precedence than top of stack pop top of the in the stack
                {                           
                    //post.add(ll.pop());
                    b += ll.pop().toString().charAt(0);
                    ll.push(a.charAt(i));
                }
                   else // if it is not lower or equal push the operator
                {
                    ll.push(a.charAt(i));
                }
            }
            else if(a.charAt(i) == '(') //if left parenthesis push onto stack
            {
                ll.push(a.charAt(i));
            }
            else if(a.charAt(i) == ')')// if right parenthesis pop everything in stack until matching left parenthesis
            {                       
                while(ll.peek() != '(')
                {
                    //post.add(ll.pop());
                    b += ll.pop().toString().charAt(0);                        
                }
                ll.pop(); //removes the left parenthesis
            }            
            else //not an operator add to list
            {
                //post.add(b[i]); //if its not an operator add it to array list                                            
                b += a.charAt(i);
            }                            
        }
        if(!ll.isEmpty()) //if stack is not empty by the end of the expression pop exerything in the stack
        {
            for(int j = -1; j < ll.size(); j++)
            {
                //post.add(ll.pop()); //add whats left in the stack
                b += ll.pop().toString().charAt(0);
            }
        }
        return b;
    }
    
    public static int Comp(char a)
    {
        //pre: is an operator
        //post: to return the value of precedence
        switch(a)
        {
            case '+': return 1; 
            case '-': return 1;
            case '/': return 2;
            case '*': return 2;
            default: return -1;    
        }
            
    }
    public static boolean isOp(char a)
    {
        //Pre: char exist
        //post: determine if char is an operator or not
        switch(a)
        {
            case '+': return true; 
            case '-': return true;
            case '/': return true;
            case '*': return true;
            default: return false;
        }
    }
    public static int compute(String a)
    {
        //Pre: a legal expression
        //Post: to compute and return the result
        Stack<Integer> ll = new Stack<>();      
        int result = 0;                         // to hold the result
        for(int i = 0; i < a.length(); i++)
        {
            if(isOp(a.charAt(i)))  //if operator is found pop top 2 things in stack and compute
            {
                
                int r = ll.pop();                
                int l = ll.pop();                
                switch(a.charAt(i))
                {
                    case '+': result = l+r; 
                    ll.push(result);
                    break;
                    case '-': result = l-r; 
                    ll.push(result);
                    break;
                    case '*': result = l*r; 
                    ll.push(result);
                    break;
                    case '/': result = l/r; 
                    ll.push(result);
                    break;
                }
            }
            else //if not operator convert value of char to int and push to stack
            {
                ll.push(Integer.parseInt(String.valueOf(a.charAt(i))));
            }
        }
        result = ll.pop(); //result equals final result
        return result;
    }
    public static void Check(String a)
    {
        //Pre: string expression
        //post: checks if expression contains errors
        int lp = 0;
        int rp = 0;
        if(isOp(a.charAt(a.length() - 1)))
        {
            System.out.println("Error equation ends with operator");
            System.exit(0);
        }
        else if(a.contains("."))
        {
           System.out.println("Error in expression!! Cannot accept floating numbers");
           System.exit(0);
        }
        for(int i = 0; i < a.length(); i++)
        {
            if(isOp(a.charAt(i)) && isOp(a.charAt(i + 1)))
            {
                System.out.println("Error " + a.charAt(i) +" operator cannot be preceded by " + a.charAt(i + 1) + " operator");
                System.exit(0);
                break;
            }
            else if(a.charAt(i) == '(' && isOp(a.charAt(i + 1)))
            {
                System.out.println("Error: no operand bewtween left parenthesis and operator");
                System.exit(0);
            }
            if(a.charAt(i) == '(') //adds 1 to the count of left parenthesis
            {
               lp++; 
            }
            if(a.charAt(i) == ')') //adds 1 to the count of right parenthesis
            {
                rp++;
            }
        }
        if(lp != rp)
        {
            System.out.println("Error: left parenthesis doesnt have matching right parenthesis or vice versa");
            System.exit(0);
        }
        
    }
}



